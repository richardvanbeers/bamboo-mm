export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/aws/bin:/root/bin
curl --silent --show-error --retry 5 https://bootstrap.pypa.io/get-pip.py | python
pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz
cfn-init --stack ${AWSStackName} --resource ${AWSResourceName} -v  --region ${AWSRegion}
cfn-signal -e ${PIPESTATUS[0]} --stack ${AWSStackName} --resource ${AWSResourceName} --region ${AWSRegion}
