from troposphere import (
    Template, GetAZs, Select, Ref, Parameter, Base64,
    Join, GetAtt, Output, Not, Equals, If, Tags
)
from util import *
from troposphere import iam, ec2, efs, ecr
from troposphere.s3 import Bucket, PublicRead
from troposphere.rds import DBInstance, DBSubnetGroup

from awacs.aws import (Allow, Policy, Principal, Statement)
from awacs.sts import (AssumeRole)

from rvb.networking import Zone
import os.path

userdata_template = userdata_file_path('bamboo-server')
userdata_references = [Ref('AWS::Region'), Ref('AWS::StackName')]

t = Template()

t.add_description("CF Troposphere template")
t.add_version("2010-09-09")

instance_key = t.add_parameter(Parameter(
    "InstanceKey",
    Type="AWS::EC2::KeyPair::KeyName",
    Description="pick keypair to use, richard is default! change if you are not me!",
    Default="rvb-test",
))

volume_size = t.add_parameter(Parameter(
    "VolumeSize",
    Type="String",
    Default="8",
    Description="Size of the bamboohome volume"
))

instance_type = t.add_parameter(Parameter(
    "InstanceType",
    Type="String",
    Default="m3.medium",
    AllowedValues=["m3.medium", "c4.large"],
    Description="Instance types",
))

docker_instance_type = t.add_parameter(Parameter(
    "DockerInstanceType",
    Type="String",
    Default="m3.medium",
    AllowedValues=["m3.medium", "c4.large"],
    Description="Instance types",
))

management_ip = t.add_parameter(Parameter(
    "ManagementIP",
    Type="String",
    Description="Your white listed IP"
))

bamboo_ami = t.add_parameter(Parameter(
    "BambooAmi",
    Type="String",
    Description="Ami",
    Default="ami-3e47e65e"
))

docker_ami = t.add_parameter(Parameter(
    "DockerAmi",
    Type="String",
    Description="Docker Ami",
    Default="ami-3e49e85e"
))

existing_bamboohome_snapshot = t.add_parameter(Parameter(
    "ExistingBambooHomeSnapshot",
    Type="String",
    Description="Previous snapshot or leave empty for a new volume",
    Default=""
))

existing_rds_snapshot = t.add_parameter(Parameter(
    "ExistingRdsSnapshot",
    Type="String",
    Description="Previous snapshot",
    Default=""
))

existing_s3_bucket = t.add_parameter(Parameter(
    "ExistingS3Bucket",
    Type="String",
    Description="Your existing backup bucket, leave empty to create a new "
                "bucket",
    Default=""
))

# go_pipelines = t.add_resource(efs.FileSystem(
#     "GoPipelines"
# ))

az = 0
create_bucket = "CreateBucket"

t.add_condition(
    create_bucket,
    Not(Equals(Ref(existing_s3_bucket),
               existing_s3_bucket.Default))
)

backup_s3_bucket = t.add_resource(Bucket(
    "BackupS3Bucket",
    Condition=create_bucket,
    DeletionPolicy="Retain"
))

actual_bucket_name = If(
    create_bucket,
    Ref(backup_s3_bucket),
    Ref(existing_s3_bucket),
)

create_rds_from_snapshot = "CreateRdsFromSnapshot"

t.add_condition(
    create_rds_from_snapshot,
    Not(Equals(Ref(existing_rds_snapshot),
               existing_rds_snapshot.Default))
)

create_volume_from_snapshot = "CreateVolumeFromSnapshot"

t.add_condition(
    create_volume_from_snapshot,
    Not(Equals(Ref(existing_bamboohome_snapshot),
               existing_bamboohome_snapshot.Default))
)

ebs_volume = t.add_resource(
    ec2.Volume(
        "BamboohomeVolume",
        AvailabilityZone=Select(az, GetAZs()),
        DeletionPolicy="Snapshot",
        Size=Ref(volume_size),
        SnapshotId=If(
            create_volume_from_snapshot,
            Ref(existing_bamboohome_snapshot),
            Ref('AWS::NoValue')))
)

bamboo_repository = t.add_resource(ecr.Repository(
    "BambooRegistry"
))

bamboo_repository_arn = Join(
    ":",
    ["arn:aws:ecr",
     Ref("AWS::Region"),
     Ref("AWS::AccountId"),
     Join("/", ["repository", Ref(bamboo_repository)])]
)

docker_role = t.add_resource(iam.Role(
    "DockerRole",
    AssumeRolePolicyDocument=Policy(
        Version="2012-10-17",
        Statement=[
            Statement(
                Effect=Allow,
                Action=[AssumeRole],
                Principal=Principal("Service", ["ec2.amazonaws.com"])
            )
        ]
    ),
    Policies=[
        iam.Policy(
            PolicyName="ECRPolicy",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Effect": "Allow",
                    "Action": [
                        "ecr:*"
                    ],
                    "Resource": "*"
                }],
            }
        ),
        iam.Policy(
            PolicyName="S3Policy",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Effect": "Allow",
                    "Action": [
                        "s3:*"
                    ],
                    "Resource": "*"
                }]
            }
        )
    ]
))
# goal ex.
# : "arn:aws:ecr:us-west-2:632826021673:repository/rvbgo-goreg-ncln6u7mjdb6"


docker_instanceprofile = t.add_resource(iam.InstanceProfile(
    "Dockerprofile",
    Roles=[Ref(docker_role)]
))

my_vpc = t.add_resource(ec2.VPC(
    "BambooVpc",
    CidrBlock="10.0.0.0/16",
    EnableDnsSupport=True,
    EnableDnsHostnames=True
))

public_sg = t.add_resource(ec2.SecurityGroup(
    "BambooSG",
    VpcId=Ref(my_vpc),
    GroupDescription="bamboo test SG",
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="22",
            ToPort="22",
            CidrIp=Ref(management_ip),
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="8085",
            ToPort="8085",
            CidrIp=Ref(management_ip),
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="80",
            ToPort="80",
            CidrIp=Ref(management_ip)
        )
    ]
))

bamboo_db_sg = t.add_resource(ec2.SecurityGroup(
    "BambooDbSG",
    VpcId=Ref(my_vpc),
    GroupDescription="bamboo db SG",
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5432",
            ToPort="5432",
            CidrIp=Ref(management_ip),
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5432",
            ToPort="5432",
            SourceSecurityGroupId=Ref(public_sg),
        )
    ]
))

public_mount_target_sg = t.add_resource(ec2.SecurityGroup(
    "MountTargetSG",
    VpcId=Ref(my_vpc),
    GroupDescription="efs SG",
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="2049",
            ToPort="2049",
            SourceSecurityGroupId=Ref(public_sg)
            # CidrIp=Ref(management_ip),
        ),
    ]
))

public_zone = Zone(public=True)

mysubnets = []

for k, v in [('a', 0), ('b', 1), ('c', 2)]:
    public_zone.subnets.append(t.add_resource(ec2.Subnet(
        "PublicSubnet{}".format(k.capitalize()),
        AvailabilityZone=Select(v, GetAZs()),
        CidrBlock="10.0.{}.0/24".format(v),
        MapPublicIpOnLaunch=public_zone.public,
        VpcId=Ref(my_vpc),
    )))
    mysubnets.append(Ref(public_zone.subnets[-1]))
    # public_zone.efs_mount_targets.append(t.add_resource(efs.MountTarget(
    #     "PublicMountTarget{}".format(k.capitalize()),
    #     FileSystemId=Ref(go_pipelines),
    #     SubnetId=Ref(public_zone.subnets[-1]),
    #     SecurityGroups=[Ref(public_mount_target_sg)]
    # )))

my_igw = t.add_resource(ec2.InternetGateway(
    "BambooIgw",
))

my_igw_attachement = t.add_resource(ec2.VPCGatewayAttachment(
    "BambooIgwAttachment",
    VpcId=Ref(my_vpc),
    InternetGatewayId=Ref(my_igw),
))

route_table = t.add_resource(ec2.RouteTable(
    "RouteTable",
    VpcId=Ref(my_vpc),
))

public_route = t.add_resource(ec2.Route(
    "PublicRoute",
    DependsOn=[my_igw_attachement.title],
    DestinationCidrBlock="0.0.0.0/0",
    GatewayId=Ref(my_igw),
    RouteTableId=Ref(route_table),
))

for s in public_zone.subnets:
    t.add_resource(ec2.SubnetRouteTableAssociation(
        "Assoc{}".format(s.title),
        RouteTableId=Ref(route_table),
        SubnetId=Ref(s),
    ))

bamboo_dbsubnetgroup = t.add_resource(DBSubnetGroup(
    "BambooDBSubnetGroup",
    DBSubnetGroupDescription="Subnets available for the RDS DB Instance",
    SubnetIds=mysubnets,
))

# bamboo_db = t.add_resource(DBInstance(
#     "BambooDB",
#     Engine="postgres",
#     MasterUsername="bamboo",
#     MasterUserPassword="rdsbamboo",
#     AllocatedStorage="5",
#     DBInstanceClass="db.t2.small",
#     DBName="bamboo",
#     DBSubnetGroupName=Ref(bamboo_dbsubnetgroup),
#     VPCSecurityGroups=[Ref(bamboo_db_sg)],
#     PubliclyAccessible=True,
#     MultiAZ=True,
# ))

bamboo_db = t.add_resource(DBInstance(
    "BambooDB",
    Condition=create_rds_from_snapshot,
    PubliclyAccessible=True,
    MultiAZ=True,
    DBInstanceClass="db.t2.small",
    DBInstanceIdentifier="bamboodb2",
    DBSnapshotIdentifier=Ref(existing_rds_snapshot),
    DBSubnetGroupName=Ref(bamboo_dbsubnetgroup),
    VPCSecurityGroups=[Ref(bamboo_db_sg)],
))

# DBSnapshotIdentifier=If(
#         create_rds_from_snapshot,
#         Ref(existing_rds_snapshot),
#         Ref('AWS::NoValue'),
# )


mount_point = "/bamboo"
mount_options = ""
mount_cmd = "mount"
mount_target = "/dev/xvdf"
full_mount_cmd = Join(" ", [
    mount_cmd,
    mount_options,
    mount_target,
    mount_point
])

bamboo_instance = t.add_resource(ec2.Instance(
    "InstanceBamboo",
    Tags=Tags(Name="BambooInstance"),
    Metadata=metadata(
        backup_bucket=actual_bucket_name,
        mount_point=mount_point,
        mount_cmd=full_mount_cmd,
        device=mount_target
    ),
    InstanceType=Ref(instance_type),
    KeyName=Ref(instance_key),
    ImageId=Ref(bamboo_ami),  # Amazon Linux AMI + java 1.8 + psql
    IamInstanceProfile=Ref(docker_instanceprofile),
    SecurityGroupIds=[Ref(public_sg)],
    SubnetId=Ref(public_zone.subnets[az]),
    Volumes=[
        ec2.MountPoint(
            VolumeId=Ref(ebs_volume),
            Device=mount_target
        )]
))

docker_instance = t.add_resource(ec2.Instance(
    "InstanceDocker",
    Tags=Tags(Name="DockerInstance"),
    InstanceType=Ref(docker_instance_type),
    KeyName=Ref(instance_key),
    ImageId=Ref(docker_ami),
    IamInstanceProfile=Ref(docker_instanceprofile),
    SecurityGroupIds=[Ref(public_sg)],
    SubnetId=Ref(public_zone.subnets[az]),

))

t.add_output(Output(
    "BambooInstancePubIp",
    Value=GetAtt(bamboo_instance, "PublicIp")
))

t.add_output(Output(
    "BambooInstancePubDns",
    Value=GetAtt(bamboo_instance, "PublicDnsName")
))

t.add_output(Output(
    "DockerInstancePubDns",
    Value=GetAtt(docker_instance, "PublicDnsName")
))

t.add_output(Output(
    "RDS",
    Value=GetAtt(bamboo_db, "Endpoint.Address")
))

t.add_output(Output(
    "S3BucketName",
    Value=actual_bucket_name
))

bamboo_instance.UserData = userdata_from_file(
    userdata_template,
    references=userdata_references,
    constants=[('AWSResourceName', bamboo_instance.title)]
)

# goal = "mount -t nfs4 -o  $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).file-system-ID.efs.aws-region.amazonaws.com:/ efs"

# pprint(vars(Ref(repository)))
#
# pprint(vars(r_arn))
# pprint(r_arn)


print t.to_json()
